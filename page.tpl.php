<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
  <head>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <title><?php print $head_title ?></title>
   <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

  <body>
    <div class="wrapper">
      <div class="container">
        <div id="secondary_nav">
		      <?php print theme('links', $secondary_links, array('class' => 'links', 'id' => 'subnavlist')) ?>
		</div>
		
        <div class="logo">
		      <?php if ($logo) : ?>
        <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" border="0" width="140" height="98" /></a>
         <?php endif; ?>
        </div>
        <div id="title">
			  <h1>
			  <?php if ($site_name) : ?>
			   <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print($site_name) ?></a>
					  <?php endif;?>
				</h1>
			  <h2>
				  <?php if ($site_slogan) : ?><?php print($site_slogan) ?>
				  <?php endif;?>
			  </h2>
	        <div id="searchbar">
		   <?php print $search_box ?>
		</div>
    </div>

		<div id="navigation">
			<?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?>
       </div>
	   <?php print $header ?>
        <br class="clear" />
        <div id="body">
          <div class="sidebar">
			<div class="content">
				<div class="links">
				<?php if ($sidebar_left != ""): ?>
				  <?php print $sidebar_left ?>
				<?php endif; ?>
				</div>
			</div>
			<br />
			<br />

			<br class="clear" />
          </div>
          
		  <div class="content">
			  <?php if ($mission != ""): ?>
			  <div id="mission"><?php print $mission ?></div>
			  <?php endif; ?>
             <?php if ($title != ""): ?>
          <?php print $breadcrumb ?>
          <h1 class="title"><?php print $title ?></h1>

          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>

        <?php endif; ?>

        <?php if ($help != ""): ?>
            <div id="help"><?php print $help ?></div>
        <?php endif; ?>

        <?php if ($messages != ""): ?>
          <?php print $messages ?>
        <?php endif; ?>

      <!-- start main content -->
      <?php print $content; ?>
      <?php print $feed_icons; ?>
      <!-- end main content -->


            <br class="clear" />
        </div>
        <br class="clear" />
      </div>
      <br class="clear" />
      <div id="footer">
		<?php if ($footer_message) : ?>
			<?php print $footer_message;?>
		<?php endif; ?>
		  <div class="clear"></div>
          <div id="copyright">
            <div class="container">
              Theme By: <a href="http://mydrupal.com">My Drupal</a>. Sponsored by: <a href="http://itdiscover.com">IT Discover</a> | <a href="http://techjobs.co.in">Tech Jobs</a> | <a href="http://csqa.info">CSQA</a>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php print $closure;?>
  </body>
</html>